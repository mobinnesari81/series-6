package sbu.cs.multithread.priority;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class Runner {

    public static List<Message> messages = new ArrayList<>();

    /**
     * add your codes to this function. this function is the caller function which will be called first.
     * changing other codes in this function is allowed.
     *
     * @param blackCount    number of black threads
     * @param blueCount     number of blue threads
     * @param whiteCount    number of white threads
     */
    public void run(int blackCount, int blueCount, int whiteCount) throws InterruptedException {
        List<ColorThread> colorThreads = new ArrayList<>();
        // your codes here
        CountDownLatch CDLB = new CountDownLatch(blackCount);
        for (int i = 0; i < blackCount; i++) {
            BlackThread blackThread = new BlackThread(CDLB);
            colorThreads.add(blackThread);
            blackThread.start();
        }
        try
        {
            CDLB.await();
        }
        catch (InterruptedException e)
        {
            System.out.println(e);
        }
        CountDownLatch CDLB2 = new CountDownLatch(blueCount);
        for (int i = 0; i < blueCount; i++) {
            BlueThread blueThread = new BlueThread(CDLB2);
            colorThreads.add(blueThread);
            blueThread.start();
        }
        try
        {
            CDLB2.await();
        }
        catch (InterruptedException e)
        {
            System.out.println(e);
        }
        CountDownLatch CDLW = new CountDownLatch(whiteCount);
        for (int i = 0; i < whiteCount; i++) {
            WhiteThread whiteThread = new WhiteThread(CDLW);
            colorThreads.add(whiteThread);
            whiteThread.start();
        }
        try
        {
            CDLW.await();
        }
        catch (InterruptedException e)
        {
            System.out.println(e);
        }
    }

    synchronized public static void addToList(Message message) {
        messages.add(message);
    }

    public List<Message> getMessages() {
        return messages;
    }

    /**
     *
     * @param args
     */
    public static void main(String[] args) {

    }
}
