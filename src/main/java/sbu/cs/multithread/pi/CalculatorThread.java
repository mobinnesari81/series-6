package sbu.cs.multithread.pi;

import ch.obermuhlner.math.big.BigDecimalMath;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class CalculatorThread implements Runnable{
    private int powerIndex;
    private int floatingPoint;

    public CalculatorThread(int powI, int floatingPoint)
    {
        this.powerIndex = powI;
        this.floatingPoint = floatingPoint;
    }

    @Override
    public void run() {
        MathContext option = new MathContext(10000, RoundingMode.HALF_DOWN);
        final BigDecimal numberTwo = new BigDecimal(2,option);
        BigDecimal firstExpresion = BigDecimalMath.pow(numberTwo, powerIndex,option);
        BigDecimal secondExpresion = BigDecimalMath.factorial(powerIndex);
        secondExpresion = secondExpresion.pow(2);
        BigDecimal thirdExpresion = BigDecimalMath.factorial(2*powerIndex+1);
        BigDecimal answer = firstExpresion.multiply(secondExpresion);
        answer = answer.divide(thirdExpresion,option);
        NumberPi.Updater(answer);
    }
}
