package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class NumberPi {
    private static BigDecimal pi = new BigDecimal(0, new MathContext(10000,RoundingMode.HALF_DOWN));

    synchronized public static void Updater(BigDecimal number)
    {
        pi = pi.add(number);
    }

    public static BigDecimal getPi()
    {
        return pi.multiply(BigDecimal.valueOf(2));
    }
}
