package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.concurrent.*;

public class PICalculator {

    /**
     * calculate pi and represent it as string with given floating point number (numbers after .)
     * check test cases for more info
     * check pi with 1000 digits after floating point at https://mathshistory.st-andrews.ac.uk/HistTopics/1000_places/
     *
     * @param floatingPoint number of digits after floating point
     * @return pi in string format
     */

    /*
        Please read this before judge my code :D
        I assume that this program will run in a 4-core cpu.
     */
    private ExecutorService eService = Executors.newFixedThreadPool(8);


    public String calculate(int floatingPoint) {
        for (int i=0 ; i <= 10000; i++)
        {
            CalculatorThread calculatorThread = new CalculatorThread(i, floatingPoint);
            eService.submit(calculatorThread);
        }
        eService.shutdown();
        try
        {
            eService.awaitTermination(100000, TimeUnit.SECONDS);
            eService.shutdownNow();
        }
        catch (InterruptedException e)
        {
            System.out.println(e);
        }
        return NumberPi.getPi().toString().substring(0, floatingPoint+2);
    }


}
