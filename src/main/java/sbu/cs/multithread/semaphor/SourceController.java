package sbu.cs.multithread.semaphor;


import java.util.concurrent.Semaphore;

public class SourceController {

    public static void main(String[] args) {
        Semaphore sem = new Semaphore(2);
        Chef chef1 = new Chef("chef1", sem);
        Chef chef2 = new Chef("chef2", sem);
        Chef chef3 = new Chef("chef3", sem);
        Chef chef4 = new Chef("chef4", sem);
        Chef chef5 = new Chef("chef5", sem);
        chef1.setPriority(1);
        chef2.setPriority(1);
        chef3.setPriority(1);
        chef4.setPriority(1);
        chef5.setPriority(1);
        chef1.start();
        chef2.start();
        chef3.start();
        chef4.start();
        chef5.start();
    }
}
