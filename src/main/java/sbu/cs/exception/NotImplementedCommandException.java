package sbu.cs.exception;

public class NotImplementedCommandException extends ApException{
    public NotImplementedCommandException() {
        super("Command is not implemented!");
    }
}
