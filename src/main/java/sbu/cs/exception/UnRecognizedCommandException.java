package sbu.cs.exception;

public class UnRecognizedCommandException extends ApException{
    public UnRecognizedCommandException() {
        super("This command is unrecognizable!");
    }
}
