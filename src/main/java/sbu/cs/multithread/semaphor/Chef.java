package sbu.cs.multithread.semaphor;

import java.util.concurrent.Semaphore;

public class Chef extends Thread {

    private Semaphore sem;

    public Chef(String name, Semaphore sem) {
        super(name);
        this.sem = sem;
    }

    @Override
    public void run() {
        try {
            sem.acquire();
            for (int i = 0; i < 10; i++) {
                Source.getSource();
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            sem.release();
        }
        catch (InterruptedException ex)
        {
            System.out.println(ex);
        }
    }
}
