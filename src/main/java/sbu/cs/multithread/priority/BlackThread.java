package sbu.cs.multithread.priority;

import java.util.concurrent.CountDownLatch;

public class BlackThread extends ColorThread {

    private static final String MESSAGE = "hi blues, hi whites";
    private CountDownLatch countDownLatch;

    void printMessage() {
        super.printMessage(new Message(this.getClass().getName(), getMessage()));
    }

    public BlackThread(CountDownLatch cdl)
    {
        countDownLatch = cdl;
    }

    @Override
    String getMessage() {
        return MESSAGE;
    }

    @Override
    public void run() {
        printMessage();
        countDownLatch.countDown();
    }
}
