package sbu.cs.exception;

import java.util.ArrayList;
import java.util.List;

public class Reader {

    /**
     * declare 2 Exception class. 1 for UnrecognizedCommand and 1 for NotImplementedCommand
     * iterate on function inputs and check for commands and throw exception when needed.
     *
     * @param args
     */
    public void readTwitterCommands(List<String> args) {
        for(String arg: args)
        {
            if(!isImplemented(arg))
            {
                if (!isAvailable(arg))
                {
                    throw new UnRecognizedCommandException();
                }
                else
                {
                    throw new NotImplementedCommandException();
                }
            }
            else
            {
                continue;
            }
        }
    }

    private boolean isAvailable(String command)
    {
        List<String> availableCommands = Util.getNotImplementedCommands();
        for (String s: availableCommands) {
            if (s.equals(command)) {
                return true;
            }
        }
        return false;
    }

    private boolean isImplemented(String command)
    {
        List<String> implementedCommands = Util.getImplementedCommands();
        for (String s: implementedCommands)
        {
            if (s.equals(command))
            {
                return true;
            }
        }
        return false;
    }
    /**
     * function inputs are String but odd positions must be integer parsable
     * a valid input is like -> "ap", "2", "beheshti", "3992", "20"
     * throw BadInput exception when the string is not parsable.
     *
     * @param args
     */
    public void read(String...args) {
        for(int i = 0 ; i < args.length; i++)
        {
            if (i % 2 == 1)
            {
                try {
                    int n = Integer.parseInt(args[i]);
                }
                catch (Exception e)
                {
                    throw new BadInputException();
                }
            }
        }
    }
}
