package sbu.cs.multithread.priority;

import java.util.concurrent.CountDownLatch;

public class BlueThread extends ColorThread {

    private static final String MESSAGE = "hi back blacks, hi whites";
    private CountDownLatch countDownLatch;

    void printMessage() {
        super.printMessage(new Message(this.getClass().getName(), getMessage()));
    }

    @Override
    String getMessage() {
        return MESSAGE;
    }

    public BlueThread(CountDownLatch cdl)
    {
        this.countDownLatch = cdl;
    }

    @Override
    public void run() {
        // call printMessage
        printMessage();
        this.countDownLatch.countDown();
    }
}
